<?php
/**
 * @author      Nick Butler <nick.butler@studioworx.co.uk>
 * @website     http://www.studioworx.co.uk
 * 
 * This script integrates with order fulfilment provider Parcelship
 * It prepares all 'new' orders and puts them into an Array().
 * It then sends the object to a cURL service provided by the 
 * integration system, and upon receiving a 'success' message, 
 * it updates the order status' to 'processing'.
 *
 */

// Need to call this script with cron like so... (every hour at 55 past, from 6 to 6, so 06:55, 07:55, 08:55 etc... up to 18:55)
// 55      6-18    *       *       *       /public_html/parcelship_export_orders.php?pword=KiiiDzT0y5ar3FUn

// secure the script 
if ($_GET['pword']!="KiiiDzT0y5ar3FUn") {
  header("Location: http://www.happykiddies.co.uk");
  die("Access denied");
}

// include the Magento core
define('MAGENTO_ROOT', getcwd());
$mageFilename = MAGENTO_ROOT . '/app/Mage.php';

require_once($mageFilename);
umask(0);
Mage::app();

// resources
$clientName = 'happykiddies';
$clientID = 51;
$clientAPI_Key = 'HKS1255874HKS';
$orderIds = array();
$theDate = date('D, F jS, Y')." at ".date('H:i:s');
$orderStatuses = array('parcelship','processing');
$orderDespatchMsg = 'Automatically send order for Parcelship fulfilment.';
$orderSkipMsg = 'Submission to Parcelship failed, skipping order';
$fields_string = '';
$itemfields_string = '';

###############################
//     working code below    //
###############################

// get pending orders
$orders = Mage::getModel('sales/order')->getCollection()->addFieldToFilter('status', array('in' => $orderStatuses));

// create new SimpleXMLElement object, so we can send an XML object via curl later on
$poArray = new SimpleXMLElement("<ArrayOfPurchaseOrder></ArrayOfPurchaseOrder>");

// function to remove accents
function stripAccents($stripAccents){
  return strtr($stripAccents,'àáâãäçèéêëìíîïñòóôõöùúûüýÿÀÁÂÃÄÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝ','aaaaaceeeeiiiinooooouuuuyyAAAAACEEEEIIIINOOOOOUUUUY');
}

##########
# ITERATE
##########

foreach ($orders as $order) {
  // order details
  $orderId = $order->getEntityId();
  $orderState = $order->getState();
  $orderPayMethod = $order->getPayment()->getMethodInstance()->getTitle();
  $orderComments = $order->getAllStatusHistory();
  $processCurrentLoop = 'yes';
  
  // addresses
  $shippingAddress = $order->getShippingAddress();
  $billingAddress = $order->getBillingAddress();
  
  // shipping method (needs correct values adding later, for now use fixed shipping code provided by Parcelship)
  /*
  if (strpos($order->getShippingDescription(),'UK Next Day') !== false) {
    $shippingMethod = 'DPD_UK';
  } else {
    $shippingMethod = ($shippingAddress->getCountry_id() == 'GB') ? 'STANDARD_UK' : 'STANDARD_OS';
  }
  */
  $shippingMethod = 1; // temp code provided by Parcelship

  // telephone number
  $orderPhoneNumber = (strlen($shippingAddress->getTelephone()) > 1) ? $shippingAddress->getTelephone() : $order->getPhone();

  // we don't want to re-send orders for fulfilment ;)
  foreach ($orderComments as $comment) {
    $body = $comment->getData('comment');
    if (strpos(strtolower($body),strtolower($orderDespatchMsg)) !== false) {
      // exit the foreach loop(s) (for this iteration)
      $processCurrentLoop = 'no';
    }
  }
  
  // don't proess this loop if it has been processed
  if ($processCurrentLoop == 'yes') {
  
    ##################
    # BUILD FIELDS OBJECT
    ##################
  
    // get all items
    $items = $order->getItemsCollection();
    $sku_post = '';
    $totalQTY = 0;
    $_catalog = Mage::getModel('catalog/product');

    // loop through the order items
    foreach ($items AS $itemid => $item) {
      $_productId = $_catalog->getIdBySku($item->getSku());
      $_product = Mage::getModel('catalog/product')->load($_productId);
        $itemfields_string = "?sku_post[]=".$item->getSku()."&qty_post[]=".round($item->getQtyOrdered(), 0)."client_id=".$clientID."&clientid=".$clientID."&api_key=".$clientAPI_Key."&end=true";
        // check SKU is valid via API call
        $ch = curl_init();
        $content = '';
        $curl_url = "http://212.84.73.213/~dev/ParcelShip/api.php".$itemfields_string;
        curl_setopt($ch, CURLOPT_URL, $curl_url);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 5.1) AppleWebKit/535.6 (KHTML, like Gecko) Chrome/16.0.897.0 Safari/535.6'); 
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 6000);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        //curl_setopt($ch,CURLOPT_POSTFIELDS, $itemfields_string);
        $content = curl_exec($ch);
        curl_close($ch);

        sleep(1);
        echo $itemfields_string.'<br><br>RESULT: '.$content.'<br><br>';
    }
  }
}
?>