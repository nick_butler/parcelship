<?php
/**
 * @author      Nick Butler <nick.butler@studioworx.co.uk>
 * @website     http://www.studioworx.co.uk
 * 
 * This script integrates with order fulfilment provider Parcelship
 * It prepares all 'new' orders and puts them into an XML object.
 * It then sends the object to a cURL service provided by the 
 * integration system, and upon receiving a 'success' message, 
 * it updates the order status' to 'processing'.
 *
 */

// call this script with cron like so... (every hour at 55 past, from 6 to 6, so 06:55, 07:55, 08:55 etc... up to 18:55)
// 55      6-18    *       *       *       /var/www/html/parcelship_export_orders.php?pword=KiiiDzT0y5ar3FUn

// secure the script 
if ($_GET['pword']!="KiiiDzT0y5ar3FUn") {
  header("Location: http://www.happykiddies.co.uk");
  die("Access denied");
}

// include the Magento core
define('MAGENTO_ROOT', getcwd());
$mageFilename = MAGENTO_ROOT . '/app/Mage.php';

require_once($mageFilename);
umask(0);
Mage::app();

// resources
$clientName = 'happykiddies';
$clientID = 51;
$clientAPI_Key = 'HKS1255874HKS';
$orderIds = array();
$theDate = date('D, F jS, Y')." at ".date('H:i:s');
$orderStatuses = array('pending','processing');
$orderDespatchMsg = 'Automatically sent order for fulfilment.';
$fields_string = '';

// working code below
###############################

// get pending orders
$orders = Mage::getModel('sales/order')->getCollection()->addFieldToFilter('status', array('in' => $orderStatuses));

// create new SimpleXMLElement object, so we can send an XML object via curl later on
$poArray = new SimpleXMLElement("<ArrayOfPurchaseOrder></ArrayOfPurchaseOrder>");

// function to remove accents
function stripAccents($stripAccents){
  return strtr($stripAccents,'àáâãäçèéêëìíîïñòóôõöùúûüýÿÀÁÂÃÄÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝ','aaaaaceeeeiiiinooooouuuuyyAAAAACEEEEIIIINOOOOOUUUUY');
}

##########
# ITERATE
##########

foreach ($orders as $order) {
  // order details
  $orderId = $order->getEntityId();
  $orderState = $order->getState();
  $orderPayMethod = $order->getPayment()->getMethodInstance()->getTitle();
  $orderComments = $order->getAllStatusHistory();
  $processCurrentLoop = 'yes';
  
  // addresses
  $shippingAddress = $order->getShippingAddress();
  $billingAddress = $order->getBillingAddress();
  
  // shipping method (needs correct values adding later, for now use fixed shipping code provided by Parcelship)
  /*
  if (strpos($order->getShippingDescription(),'UK Next Day') !== false) {
    $shippingMethod = 'DPD_UK';
  } else {
    $shippingMethod = ($shippingAddress->getCountry_id() == 'GB') ? 'STANDARD_UK' : 'STANDARD_OS';
  }
  */
  $shippingMethod = 1; // temp code provided by Parcelship

  // telephone number
  $orderPhoneNumber = (strlen($shippingAddress->getTelephone()) > 1) ? $shippingAddress->getTelephone() : $order->getPhone();

  // we don't want to re-send orders for fulfilment ;)
  foreach ($orderComments as $comment) {
    $body = $comment->getData('comment');
    if (strpos(strtolower($body),strtolower($orderDespatchMsg)) !== false) {
      // exit the foreach loop(s) (for this iteration)
      $processCurrentLoop = 'no';
    }
  }
  
  // don't proess this loop if it has been processed
  if ($processCurrentLoop == 'yes') {
  
    ##################
    # BUILD FIELDS OBJECT
    ##################
  
    // get all items
    $items = $order->getItemsCollection();
    $sku_post = '';
    $totalQTY = 0;
    $_catalog = Mage::getModel('catalog/product');

    // loop through the order items
    foreach ($items AS $itemid => $item) {
      //$_productId = $_catalog->getIdBySku($item->getSku());
      //$_product = Mage::getModel('catalog/product')->load($_productId);
      //$_parcelship = $_product->getAttributeText('parcelship');
      //echo "Attribute ".$_productId.": ".$_parcelship."<br>";
      //if($_parcelship == 'Yes'){
        //$sku_post .= "&sku_post[]=".$item->getSku()."&qty_post[]=".round($item->getQtyOrdered(), 0);
        $sku_post = "&sku_post[]=8852031075678&qty_post[]=2";
        $totalQTY += $item->getQtyOrdered();
      //}
    }
    if($totalQTY != 0){
      $deliveryAddressNumber = explode(' ', $shippingAddress->getStreet(1));
      $deliveryAddressStreet = str_replace($deliveryAddressNumber[0].' ', '', $shippingAddress->getStreet(1));
      $fields = array(
        'ref' => $ref,
        'delivery_option' => $shippingMethod,
        'number' => $totalQTY,
        'po_number' => $orderId,
        's_name' => $shippingAddress->getPrefix().' '.stripAccents($shippingAddress->getFirstname()).' '.stripAccents($shippingAddress->getLastname()),
        's_company' => $shippingAddress->getCompany(),
        's_l1' => $deliveryAddressNumber[0],
        's_l2' => $deliveryAddressStreet,
        's_town' => $shippingAddress->getCity(),
        's_county' => $shippingAddress->getRegion(),
        's_country' => $shippingAddress->getCountry_id(),
        's_postcode' => $shippingAddress->getPostcode(),
        'email' => $shippingAddress->getEmail(),
        'tel' => $orderPhoneNumber
      );
      $fields_string = http_build_query($fields) . $sku_post.'&';
      // add the id to the order ids array
      $orderIds[] = $orderId;
    }
  }

} // end of $order for_each loop
$fields_string .= 'client_id='.$clientID.'&clientid='.$clientID.'&api_key='.$clientAPI_Key.'&end=true';

##############
# CURL REQUEST
##############

// check if we have orders, if not, don't make the curl request
if (empty($orderIds)) {
  exit("Success :: No Orders (".$theDate.").");
} else {
  echo '<pre>'.var_dump($fields_string).'</pre>';
  echo '<pre>'.var_dump($orderIds).'</pre>';

  // prepare the curl data
  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, "http://212.84.73.213/~dev/ParcelShip/api_post_method.php");
  curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 5.1) AppleWebKit/535.6 (KHTML, like Gecko) Chrome/16.0.897.0 Safari/535.6'); 
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 6000);
  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
  curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
  curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
  $content = curl_exec($ch);
  curl_close($ch);
}

// be nice - wait a second for results from curl
sleep(1);

#########
# RESULT
#########

if (strpos(strtolower($content),'success') !== false) {
  // change order status to processing
   foreach ($orderIds AS $id) {
     $order = Mage::getModel('sales/order')->load($id);
  //   $order->setIsInProcess(true);
  //   $order->addStatusHistoryComment($orderDespatchMsg, false);
  //   // update order status
  //   if ($order->getState() != Mage_Sales_Model_Order::STATE_PROCESSING){
  //     $order->setState(Mage_Sales_Model_Order::STATE_PROCESSING, true);
  //   }
  //   $order->save();
     echo "Success :: Order No. ".$id." Submitted ".$theDate.".";
   }
  exit("Success :: Orders Processed (".$theDate.").");
} else {
  echo "Error: ".$content."<p></p>";
  
  // send an email if this fails!
  $to_add = "nick.butler@studioworx.co.uk";
  //$to_add = "dan@parcelship.co.uk";
  $from_add = "Parcelship Support <happykiddies.client@dan@parcelship.co.uk>";
  $subject = "DEV TEST - Server Error - DEV TEST";
  
  $message = "Dear User, \r\nThe order export failed while sending to Parcelship from happykiddies.co.uk with following message: ".$content;
  $message .= ".\r\nCron tried to run export on ";
  $message .= date("D, F jS, Y")." at ".date("H:i:s");
  $message .= ". Please investigate the cause of this error ASAP. \r\nEnd.";
  
  $headers = "From: $from_add \r\n";
  $headers .= "Reply-To: $from_add \r\n";
  $headers .= "Return-Path: $from_add\r\n";
  $headers .= "X-Mailer: PHP \r\n";
  
  // send the email
  if (mail($to_add,$subject,$message,$headers)) {
    echo "Reporting error...";
  } else {
    echo "Email could not be sent!";
  }
  exit();
}
?>