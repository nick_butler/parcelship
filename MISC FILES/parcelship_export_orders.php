<?php
/**
 * @author      Nick Butler <nick.butler@studioworx.co.uk>
 * @website     http://www.studioworx.co.uk
 * 
 * This script integrates with order fulfilment provider Parcelship
 * It prepares all 'new' orders and puts them into an Array().
 * It then sends the object to a cURL service provided by the 
 * integration system, and upon receiving a 'success' message, 
 * it updates the order status' to 'parcelship (processing)'.
 */
// Call script via cron like so... (every hour at 55 past, from 6 to 6, so 06:55, 07:55, 08:55 etc... up to 18:55)
// 55      6-18    *       *       *       /public_html/parcelship/parcelship_export_orders.php?pword=KiiiDzT0y5ar3FUn

// secure the script 
if ($_GET['pword']!="KiiiDzT0y5ar3FUn") {
  header("Location: http://www.happykiddies.co.uk");
  die("Access denied");
}

// include the Magento core
define('MAGENTO_ROOT', getcwd());
$mageFilename = MAGENTO_ROOT . '/app/Mage.php';

require_once($mageFilename);
umask(0);
Mage::app();

// resources
$clientName = 'happykiddies';
$clientID = 51;
$clientAPI_Key = 'HKS1255874HKS';
$orderIds = array();
$ordersStartId = 12800; // set to 0 if you don't know the ID, or don't need to skip older orders
$orderStatuses = array('processing');
$orderDespatchMsg = 'Automatically send order for Parcelship fulfilment.';
$orderSkipMsg = 'Skip order as Parcelship unable to fulfill.';

######################
# working code below #
######################
// get pending orders
$orders = Mage::getModel('sales/order')->getCollection()->addFieldToFilter('status', array('in' => $orderStatuses));
// create new SimpleXMLElement object, so we can send an XML object via curl later on
$poArray = new SimpleXMLElement("<ArrayOfPurchaseOrder></ArrayOfPurchaseOrder>");
// function to remove accents
function stripAccents($stripAccents){
  return strtr($stripAccents,'àáâãäçèéêëìíîïñòóôõöùúûüýÿÀÁÂÃÄÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝ','aaaaaceeeeiiiinooooouuuuyyAAAAACEEEEIIIINOOOOOUUUUY');
}

##########
# ITERATE
##########
foreach ($orders as $order) {

  $orderId = $order->getEntityId();
  $orderState = $order->getState();
  $orderPayMethod = $order->getPayment()->getMethodInstance()->getTitle();
  $orderComments = $order->getAllStatusHistory();
  $processCurrentLoop = 'yes';
  echo "<br><hr><br>";
  echo "Order ID: ".$orderId."<br>";
  echo "Order State: ".$orderState."<br>";
  echo "Order Pay Method: ".$orderPayMethod."<br><br>";

  // don't re-send orders for fulfilment
  foreach ($orderComments as $comment) {
    $body = $comment->getData('comment');
    echo "<br>Commment: ".$body."<br>";
    if ((strpos(strtolower($body),strtolower($orderDespatchMsg)) !== false) || (strpos(strtolower($body),strtolower($orderSkipMsg)) !== false)) {
      // exit the foreach loop(s) (for this iteration)
      $processCurrentLoop = 'no';
    }; // endif
  }; // end of foreach

  // don't process this order if it has already been processed
  if (($processCurrentLoop == 'yes') && ($orderId >= $ordersStartId)) {
    
    echo "<br><br>Process Order: ".$processCurrentLoop."<br><br>";
    $theDate = date('D, F jS, Y')." at ".date('H:i:s');
    echo "<br>".$theDate."<br>";
    $fields_string = '';
    $itemfields_string = '';

    // order details
    $shippingAddress = $order->getShippingAddress();
    $billingAddress = $order->getBillingAddress();
    $shippingMethod = 1; // temp code provided by Parcelship
    $orderPhoneNumber = (strlen($shippingAddress->getTelephone()) > 1) ? $shippingAddress->getTelephone() : $order->getPhone();// telephone number
  
    #######################
    # BUILD FIELDS OBJECT #
    #######################
    // get all items
    $items = $order->getItemsCollection();
    $sku_post = '';
    $totalQTY = 0;
    $_catalog = Mage::getModel('catalog/product');

    // loop through the items
    foreach ($items AS $itemid => $item) {
      $_productId = $_catalog->getIdBySku($item->getSku());
      $productSku = $item->getSku();
      // $sku_post .= "&sku_post[]=DEVTEST1234&qty_post[]=1";
      $sku_post .= "&sku_post[]=".$productSku."&qty_post[]=".round($item->getQtyOrdered(), 0);
      $totalQTY += $item->getQtyOrdered();
    }; // End of item loop

    if($totalQTY !== 0){
      $deliveryAddressNumber = explode(' ', $shippingAddress->getStreet(1));
      $deliveryAddressStreet = str_replace($deliveryAddressNumber[0].' ', '', $shippingAddress->getStreet(1));
      $fields = array(
        'ref' => $ref,
        'delivery_option' => $shippingMethod,
        'number' => $totalQTY,
        'po_number' => $orderId,
        's_name' => $shippingAddress->getPrefix().' '.stripAccents($shippingAddress->getFirstname()).' '.stripAccents($shippingAddress->getLastname()),
        's_company' => $shippingAddress->getCompany(),
        's_l1' => $deliveryAddressNumber[0],
        's_l2' => $deliveryAddressStreet,
        's_town' => $shippingAddress->getCity(),
        's_county' => $shippingAddress->getRegion(),
        's_country' => $shippingAddress->getCountry_id(),
        's_postcode' => $shippingAddress->getPostcode(),
        'email' => $shippingAddress->getEmail(),
        'tel' => $orderPhoneNumber
      );
      $fields_string = http_build_query($fields) . $sku_post.'&';
      $orderIds[] = $orderId; // add the id to the order ids array
    }; // End of fields array building

    $fields_string .= 'client_id='.$clientID.'&clientid='.$clientID.'&api_key='.$clientAPI_Key.'&end=true';
    echo "<br><br>".$fields_string."<br><br>";

    ##############
    # CURL REQUEST
    ##############
    // check if we have orders, if not, don't make the curl request
    if ((!empty($orderIds)) && (!empty($sku_post))) {
      
      // prepare the curl data
      echo "PREPPING cURL...<br>";
      $content = '';
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, "http://212.84.73.213/~dev/ParcelShip/api_post_magento.php"); 
      curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 5.1) AppleWebKit/535.6 (KHTML, like Gecko) Chrome/16.0.897.0 Safari/535.6'); 
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 6000);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
      curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
      curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
      $content = curl_exec($ch);
      curl_close($ch);
    
      // wait a second for results from curl
      sleep(1);

      #########
      # RESULT
      #########
      if (strpos(strtolower($content),'success') !== false) {
        // change order status to processing
        //foreach ($orderIds AS $id) {
           $order = Mage::getModel('sales/order')->load($orderId);
           $order->setIsInProcess(true);
           $order->addStatusHistoryComment($orderDespatchMsg, false);
           // update order status
           if ($order->getState() != Mage_Sales_Model_Order::STATE_PROCESSING){
             $order->setState(Mage_Sales_Model_Order::STATE_PROCESSING, true);
           }
           $order->save();
          echo "Success :: Order No. ".$orderId." Submitted ".$theDate.".<br>";
        //}
        // exit("Success :: Orders Processed (".$theDate.").<br>");
      } else {
        echo "Order: ".$orderId." - Error: ".$content."<p></p>";
        
        // send an email if this fails!
        $to_add = "nick.butler@studioworx.co.uk";
        $to_add = "dan@parcelship.co.uk";
        $from_add = "Parcelship Support <happykiddies.client@parcelship.co.uk>";
        $subject = "Happy Kiddies to Parcelship - API Server Error";
        
        $message = "Dear User, \r\nThe order (".$orderId.") export failed while sending to Parcelship from happykiddies.co.uk with following message: ".$content;
        $message .= ".\r\nCron tried to run export on ";
        $message .= date("D, F jS, Y")." at ".date("H:i:s");
        $message .= ". Please investigate the cause of this error ASAP. \r\nEnd.";
        
        $headers = "From: $from_add \r\n";
        $headers .= "Reply-To: $from_add \r\n";
        $headers .= "Return-Path: $from_add\r\n";
        $headers .= "X-Mailer: PHP \r\n";
        
        // send the email
        if (mail($to_add,$subject,$message,$headers)) {
          echo "Reporting error...";
        } else {
          echo "Email could not be sent!";
        };
        // exit();
      }; // end of cURL error check
    }; // end of order/sku not empty check
  } else {
      echo "<br><br>Process Order: ".$processCurrentLoop."<br><br>";
  }; // End of process loop check
}; // end of $order for_each loop
echo "<br><br>############ END OF ALL ORDERS ############<br><br>";
echo "<br>----------<br>";
echo 'ORDERS SUBMITTED: <br>';
foreach ($orderIds as $order) {
  echo $order.'<br>';
};
echo "<br>----------<br>";
//exit();
?>