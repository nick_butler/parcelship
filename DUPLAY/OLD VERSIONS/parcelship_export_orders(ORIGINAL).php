<?php
/**
 * @author      Nick Butler <nick.butler@studioworx.co.uk>
 * @website     http://www.studioworx.co.uk
 * 
 * This script integrates with order fulfilment provider Parcelship
 * It prepares all 'new' orders and puts them into an XML object.
 * It then sends the object to a cURL service provided by the 
 * integration system, and upon receiving a 'success' message, 
 * it updates the order status' to 'processing'.
 *
 */

// call this script with cron like so... (every hour at 55 past, from 6 to 6, so 06:55, 07:55, 08:55 etc... up to 18:55)
// 55      6-18    *       *       *       /var/www/html/parcelship_export_orders.php?pword=pl4st0y123

// secure the script 
if ($_GET['pword']!="pl4st0y123") {
  header("Location: http://www.duplay.co.uk");
  die("Access denied");
}

// include the Magento core
define('MAGENTO_ROOT', getcwd());
$mageFilename = MAGENTO_ROOT . '/app/Mage.php';

require_once($mageFilename);
umask(0);
Mage::app();

// resources
$clientName = 'duplay';
$clientID = 81;
$clientAPI_Key = 'PLTDDUPLAY1234';
$orderIds = array();
$theDate = date('D, F jS, Y')." at ".date('H:i:s');
$orderStatuses = array('pending','processing');
$orderDespatchMsg = 'Automatically sent order for fulfilment.';

// working code below
###############################

// get pending orders
$orders = Mage::getModel('sales/order')->getCollection()->addFieldToFilter('status', array('in' => $orderStatuses));

// create new SimpleXMLElement object, so we can send an XML object via curl later on
$poArray = new SimpleXMLElement("<ArrayOfPurchaseOrder></ArrayOfPurchaseOrder>");

// function to remove accents
function stripAccents($stripAccents){
  return strtr($stripAccents,'àáâãäçèéêëìíîïñòóôõöùúûüýÿÀÁÂÃÄÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝ','aaaaaceeeeiiiinooooouuuuyyAAAAACEEEEIIIINOOOOOUUUUY');
}

##########
# ITERATE
##########

foreach ($orders as $order) {
  // order details
  $orderId = $order->getEntityId();
  $orderState = $order->getState();
  $orderPayMethod = $order->getPayment()->getMethodInstance()->getTitle();
  $orderComments = $order->getAllStatusHistory();
  $processCurrentLoop = 'yes';
  
  // addresses
  $shippingAddress = $order->getShippingAddress();
  $billingAddress = $order->getBillingAddress();
  
  // shipping method (needs correct values adding later, for now use fixed shipping code provided by Parcelship)
  /*
  if (strpos($order->getShippingDescription(),'UK Next Day') !== false) {
    $shippingMethod = 'DPD_UK';
  } else {
    $shippingMethod = ($shippingAddress->getCountry_id() == 'GB') ? 'STANDARD_UK' : 'STANDARD_OS';
  }
  */
  $shippingMethod = 1; // temp code provided by Parcelship

  // telephone number
  $orderPhoneNumber = (strlen($shippingAddress->getTelephone()) > 1) ? $shippingAddress->getTelephone() : $order->getPhone();
  
  // we don't want to re-send orders for fulfilment ;)
  foreach ($orderComments as $comment) {
    $body = $comment->getData('comment');
    if (strpos(strtolower($body),strtolower($orderDespatchMsg)) !== false) {
      // exit the foreach loop(s) (for this iteration)
      $processCurrentLoop = 'no';
    }
  }
  
  // don't proess this loop if it has been processed
  if ($processCurrentLoop == 'yes') {
  
    ##################
    # BUILD XML OBJECT
    ##################
  
    // add child element to the SimpleXML object
    $pOrder = $poArray->addChild('PurchaseOrder');
  
    $pOrder->addChild('CreatedBy', $clientName);
    $pOrder->addChild('CreatedOn', $order->getCreatedAt());
    $pOrder->addChild('AmendedBy', $clientName);
    $pOrder->addChild('AmendedOn', $order->getUpdatedAt());
    $pOrder->addChild('Campaign', $order->getQuoteId());
    $pOrder->addChild('client_id', 81);
    $pOrder->addChild('api_key', $clientAPI_Key);
    $pOrder->addChild('ContactNumber', $order->getCustomerId());
    $pOrder->addChild('Title', $order->getCustomerPrefix());
    $pOrder->addChild('Forename', stripAccents($order->getCustomerFirstname()));
    $pOrder->addChild('Surname', stripAccents($order->getCustomerLastname()));
    $pOrder->addChild('Company', $order->getCompany());
    $pOrder->addChild('Email', $order->getCustomerEmail());
    $pOrder->addChild('Phone', $order->getPhone());
    $pOrder->addChild('Currency', $order->getOrderCurrencyCode());
    $pOrder->addChild('ContactHistoryNumber');
    $pOrder->addChild('CurrentState', 'Confirmed');
    $pOrder->addChild('PurchaseOrderNumber', $orderId);
    $pOrder->addChild('DeliveryDate');
    $pOrder->addChild('BatchNumber');
    $pOrder->addChild('TransactionNumber', $orderId);
    $pOrder->addChild('DeliveryAddressNumber', $order->getShippingAddressId());
    $pOrder->addChild('BillingAddressNumber');
    $pOrder->addChild('IsGift');
    $pOrder->addChild('TaxAmount', $order->getTaxAmount());
    $pOrder->addChild('DiscountTotal', $order->getDiscountAmount());
    $pOrder->addChild('DeliveryAmount', $order->getShippingInclTax());
    $pOrder->addChild('DonationAmount', '0');
    $pOrder->addChild('TotalAmount', $order->getGrandTotal());
    $pOrder->addChild('MarketingInfo');
    $pOrder->addChild('ThankingCode');
    $pOrder->addChild('EmailCert');
    $pOrder->addChild('IsTest');
    $pOrder->addChild('DeliveryTitle', $shippingAddress->getPrefix());
    $pOrder->addChild('DeliveryForename', stripAccents($shippingAddress->getFirstname()));
    $pOrder->addChild('DeliverySurname', stripAccents($shippingAddress->getLastname()));
    $pOrder->addChild('DeliveryCompany', $shippingAddress->getCompany());
    $pOrder->addChild('DeliveryEmail', $shippingAddress->getEmail());
    $pOrder->addChild('DeliveryPhone', $orderPhoneNumber);
    $pOrder->addChild('DeliveryAddress', $shippingAddress->getStreet(1));
    $pOrder->addChild('DeliveryAddress2', $shippingAddress->getStreet(2));
    $pOrder->addChild('DeliveryAddress3', $shippingAddress->getStreet(3));
    $pOrder->addChild('DeliveryTown', $shippingAddress->getCity());
    $pOrder->addChild('DeliveryCounty', $shippingAddress->getRegion());
    $pOrder->addChild('DeliveryPostcode', $shippingAddress->getPostcode());
    $pOrder->addChild('DeliveryCountry', $shippingAddress->getCountry_id());
    $pOrder->addChild('DeliveryMessage');
    $pOrder->addChild('BillingAddress', $billingAddress->getStreet(1));
    $pOrder->addChild('BillingAddress2', $billingAddress->getStreet(2));
    $pOrder->addChild('BillingAddress3', $billingAddress->getStreet(3));
    $pOrder->addChild('BillingTown', $billingAddress->getCity());
    $pOrder->addChild('BillingCounty', $billingAddress->getRegion());
    $pOrder->addChild('BillingPostcode', $billingAddress->getPostcode());
    $pOrder->addChild('BillingCountry', $billingAddress->getCountry_id());
    $pOrder->addChild('ContactMethod', 'WEBS');
    $pOrder->addChild('ShipMethod', $shippingMethod);
  
    $pItems = $pOrder->addChild('PurchaseItems');
    $pItems->addAttribute('PurchaseOrderNumber', $orderId);
  
    // get all items
    $items = $order->getItemsCollection();
  
    // loop through the order items
    foreach ($items AS $itemid => $item) {
      $pItem = $pItems->addChild('PurchaseItem');
      $pItem->addChild('CreatedBy', $clientName);
      $pItem->addChild('CreatedOn', $item->getCreatedAt());
      $pItem->addChild('PurchaseItemNumber', $item->getProductId());
      $pItem->addChild('PurchaseOrderNumber', $orderId);
      $pItem->addChild('ProductCode', $item->getSku());
      $pItem->addChild('LineNumber');
      $pItem->addChild('ProductName', $item->getName());
      $pItem->addChild('ProductPrice', $item->getPrice());
      $pItem->addChild('ProductVat', $item->getTaxAmount());
      $pItem->addChild('ProductDiscount', $item->getDiscount());
      $pItem->addChild('Quantity', $item->getQtyOrdered());
    }
    // add the id to the order ids array
    $orderIds[] = $orderId;
  }
}

##############
# CURL REQUEST
##############

// check if we have orders, if not, don't make the curl request
if (empty($orderIds)) {
  exit("Success :: No Orders (".$theDate.").");
} else {
  // prepare the curl data
  $data = array();
  $data['OrderXML'] = $poArray->asXML();
  $data['submit'] = 'submit';
  $post_str = '';

  foreach ($data as $key=>$val) {
    $post_str .= $key.'='.str_replace('&','and',str_replace('&amp;','and',$val)).'&';
  }
var_dump($post_str);
  // send the object via curl
  $post_str = substr($post_str, 0, -1);
  
  // Parcelship API endpoint
  $handle = curl_init("http://212.84.73.213/~dev/ParcelShip/api_post_method.php?client_id=".$clientID."&api_key=".$clientAPI_Key);

  curl_setopt($handle, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($handle, CURLOPT_POST, 1);
  curl_setopt($handle, CURLOPT_POSTFIELDS, $post_str);
  curl_setopt($handle, CURLOPT_SSL_VERIFYPEER, false);
  
  //$content = curl_exec($handle);
  curl_close($handle);
}

// be nice - wait a second for results from curl
sleep(1);

#########
# RESULT
#########

if (strpos(strtolower($content),'success') !== false) {
  // change order status to processing
  foreach ($orderIds AS $id) {
    $order = Mage::getModel('sales/order')->load($id);
    $order->setIsInProcess(true);
    $order->addStatusHistoryComment($orderDespatchMsg, false);
    // update order status
    if ($order->getState() != Mage_Sales_Model_Order::STATE_PROCESSING){
      $order->setState(Mage_Sales_Model_Order::STATE_PROCESSING, true);
    }
    $order->save();
    echo "Success :: Order No. ".$id." Submitted ".$theDate.".
";
  }
  exit("Success :: Orders Processed (".$theDate.").");
} else {
  echo "Error: ".$content."<p></p>";
  
  // send an email if this fails!
  $to_add = "nick.butler@studioworx.co.uk";
  $from_add = "Support <duplay.client@studioworx.co.uk>";
  $subject = "Server Error";
  
  $message = "Dear User, \r\nThe order export failed while sending to Parcelship from Duplay.co.uk with following message: ".$content;
  $message .= ".\r\nCron tried to run export on ";
  $message .= date("D, F jS, Y")." at ".date("H:i:s");
  $message .= ". Please investigate the cause of this error ASAP. \r\nEnd.";
  
  $headers = "From: $from_add \r\n";
  $headers .= "Reply-To: $from_add \r\n";
  $headers .= "Return-Path: $from_add\r\n";
  $headers .= "X-Mailer: PHP \r\n";
  
  // send the email
  if (mail($to_add,$subject,$message,$headers)) {
    echo "Reporting error...";
  } else {
    echo "Email could not be sent!";
  }
  exit();
}
?>